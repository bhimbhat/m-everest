<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'travel' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'D4sb6x./:?1*4N|slNI^>}N9ivld&pY2Gz7K>^!$`2w.D``6Ce/vvV;d$53Fh^9E' );
define( 'SECURE_AUTH_KEY',  's_&:)Al{9!K)}P~vL!3LUI);yTFOYKcDeRfzeP5Q=ByWqy|%7R.#S?|$z=`#Rm!2' );
define( 'LOGGED_IN_KEY',    'CRkU/%QJQFH1uS.OVhLGXS$Nf/Nijb*FZwvBQJ+U}7<{r@VdQDk-F![`,5U{MLsK' );
define( 'NONCE_KEY',        '^cjU+aUW0bqi]1Y-nDHNkv&X>]zqobmfIl<*puoNd@?H`M:?[~:c:7M:;(fX]y26' );
define( 'AUTH_SALT',        'JK~{ge0Q|QtMW2)$:VDstuwn&P^($glk+)AaJn,D=;9kxjLFc$DCE:h>2h#O@;uq' );
define( 'SECURE_AUTH_SALT', 'kP~_<E7}NvOp3;<]APwni|>=NLY=c7tt7xo]|+}8IZSe8o$8wQY|^<Q2cV-H=WAE' );
define( 'LOGGED_IN_SALT',   's%&B_$<|TU+vU;o~!Y6A&(wi j4r6NH0b%DQ /Pye{5XN(dl[&u50L8_T{m;PgM0' );
define( 'NONCE_SALT',       '*mpgs?_F7~pu(D)cf?#cq)/sj^YY|~1ZUPS-T5W/x9XDh_u$RXKsw}]~lI`.1-XR' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
